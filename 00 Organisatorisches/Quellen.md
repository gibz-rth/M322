# Quellen

Für einige Kompetenzbereiche und Kompetenzen sind in den Unterverzeichnissen des Verzeichnis [02 Materialien](../02%20Materialien) mögliche Unterlagen für den Kompetenzerwerb vorhanden. Diese Materialien *können* verwendet werden. Dabei sind die darin enthaltenen Erklärungen und Beispiele möglicherweise nicht für alle Lernenden ausreichend, um den Kompetenzerwerb einzig damit abschliessen zu können. Für andere Lernende wiederum sind keine zusätzlichen Erklärungen notwendig. Es ist daher durchaus möglich bzw. sinnvoll bzw. ratsam, auch zusätzliche, alternative Quellen für den Kompetenzerwerb zu nutzen.

In den nachfolgenden Abschnitten sind einige mögliche Quellen aufgeführt. Die Aufzählung ist *nicht abschliessend*, d.h. es existieren zahlreiche weitere Quellen, welche für den Kompetenzerwerb hilfreich sein können.

## Wikipedia

Die freie Enzyklopädie gilt oft als umstrittenes Werkzeug, wenn es um wissenschaftliche Arbeiten geht. Für grundlegende, allgemein etablierte Konzepte der Programmierung ist dieses Online-Nachschlagewerk aber sehr gut geeignet.

## YouTube

Für das Erlernen von Programmierkonzepten bietet die Videoplattform von Google viele wertvolle Videos. Die Qualität dieser Videos reicht von fragwürdig, mittelmässig bis hin zu perfekt aufbereiteten und vermittelten Inhalten. Ebenso divers ist das Zielpublikum (Sprache, Programmiersprache, Niveau, ...). Bei der Auswahl der Videos sollte auf die "richtige Programmiersprache" (C#) sowie eine möglichst klare und gut verständliche Erklärung geachtet werden.


## Tutorials

Im Internet existieren zahlreiche Websiten und Applikationen mit Tutorials zum Erlernen der Programmiersprache C#. Dabei steht neben der Syntax in C# meist auch das objektorientierte Programmierparadigma im Vordergrund.

Bei der Nutzung solcher Tutorials ist es daher wichtig zu wissen, dass sämtlich objektorientierten Konzepte *kein* Bestandteil des Modul 319 sind. Die Definition von Klassen und Methoden sowie die Instanzierung von Objekten soll bei der Nutzung von Tutorials also ignoriert werden. Stattdessen sind beispielsweise die *inhalte der Methoden* relevant - also beispielsweise die Verwendung von Variablen, Bedingungen (Selektion), Schleifen (Iteration), ...
