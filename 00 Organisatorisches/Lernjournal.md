# Lernjournal

Der Lern- und Arbeitsprozess im Modul 322 wird weitgehend individuell durch die Lernenden geplant und durchgeführt. Die Lehrperson steht dabei unterstützend und beratend zur Seite - sowohl inhaltlich als auch organisatorisch und prozessorientiert.

Damit die Steuerung des Lernprozess durch die Lernenden sowie die Begleitung dieses Prozess' durch die Lehrperson bestmöglich erfolgen kann, führen die Lernenden während der gesamten Dauer des Modul 322 ein persönliches Lernjournal.

## Inhalte

Im Lernjournal werden alle relevanten Gedanken, Überlegungen, Planungen, Auswertungen, Befindlichkeiten und Ereignisse rund um den Lern- und Arbeitsprozess im Modul 322 schriftlich festgehalten.

So wird beispielsweise die Arbeit des vergangenen Halbtages kritisch betrachtet und reflektiert. Welche Arbeiten waren geplant und was wurde effektiv erreicht? Wie wird die persönliche Effizienz eingeschätzt? Welche Quellen, Arbeitsmittel und/oder Methoden haben sich bewährt? Was war eher schwierig, kompliziert, unnötig oder mühsam?

Aus diesen Überlegungen und mit Blick auf das gesamte Kriterienraster soll im folgenden ein Plan für den weiteren Verlauf, mit Fokus auf den jeweils nachfolgenden Halbtag erstellt erstellt werden. Darin werden die nächsten Arbeitsschritte notiert. Welche Kompetenzen sollen angepackt, vertieft oder abgeschlossen werden? Wo bzw. in welcher Form und mit welchen Hilfsmitteln wird die Erarbeitung der Kompetenzen stattfinden? Was sind die persönlichen Ziele auf inhaltlicher und organisatorischer Ebene? Was sind unterstützende bzw. hinderliche Faktoren, welche für der Zielerreichung beachtet werden sollen?

Daneben sollen immer auch spontane Befindlichkeiten, interessante Erfahrungen, überraschende und erstaunliche Inhalte sowie Verknüpfungen zu bereits vertrauten Themen notiert werden. Mit wem hat die Zusammenarbeit gut funktioniert? Welche erarbeiteten Kompetenzen waren besonders interessant? Was war bereits bekannt und konnte daher in kurzer Zeit abgeschlossen werden? Welche Themen wurden in der Sekundarstufe 1 bereits (in ähnlicher Form) behandelt?

## Form

Das persönliche Lernjournal wird jeweils am Ende eines Halbtages erstellt. Dazu reservieren sich die Lernenden ungefähr 20 Minuten am Ende der jeweiligen Unterrichtseinheit. Für das Niederschreiben der Arbeiten, Erfahrungen und Gedanken sind Stichworte, welche während der Arbeit fortlaufend im Lernjournal aufgeführt werden, hilfreich!

Das Lernjournal wird im Abschnitt *Lernjournal 322* im OneNote-Klassennotizbuch geführt. Dieses Klassennotizbuch ist über den Kanal *Allgemein* in Teams oder über die Synchronisation mit der OneNote-App auf dem persönlichen Gerät verfügbar. Es wird empfohlen, für jeden Eintrag eine neue Seite im jeweiligen Abschnitt anzulegen.

Die Lehrperson hat Einblick in das Lernjournal aller Lernenden der Klasse. Sie nutzt die Einträge im Lernjournal für die Auswertung und Begleitung des Lernprozess. Dadurch können allfällige Hürden und Schwierigkeiten frühzeitig erkannt und möglicherweise in Zusammenarbeit mit den Lernenden oder der ganzen Klasse beseitigt werden. Möglicherweise wird die Lehrperson ausgewählte Einträge mit Hinweisen für nachfolgende Einträge oder den weiteren Lernprozess versehen. Es lohnt sich daher, auch die vorhergehenden Einträge im persönlichen Lernjournal zu überfliegen.
