**INHALTSVERZEICHNIS**

[TOC]


# M322 - Benutzerschnittstellen entwerfen und implementieren

*Entwirft und implementiert Benutzerschnittstellen für eine Applikation. Beachtet dabei Standards und ergonomische Anforderungen.*

2. Lehrjahr, Q1/Q2, für Applikationsentwickler


[> **Modulidentifikation** ](https://www.modulbaukasten.ch/module/322/1/de-DE?title=Benutzerschnittstellen-entwerfen-und-implementieren)

In diesem Repository werden alle vorhandenen Unterlagen, Links und Hilfsmittel für das Modul 322 zur Verfügung gestellt. Die Inhalte werden fortlaufend ergänzt, aktualisiert und überarbeitet.

## Schnellstart

Alle relevanten Informationen für die Arbeit mit diesem Repository im Rahmen des Modul 322 am GIBZ sind in verschiedenen Dokumenten enthalten. Um einen effizienten Einstieg zu gewährleisten, wird das sorgfältige Studium der nachfolgenden Dokumente bzw. Abschnitte empfohlen:

1. [Kompetenzenraster](01%20Kompetenzenraster/Kompetenzenraster.md): Es reicht, die tabellarische Darstellung der Kompetenzen zu überfliegen und nur die beiden Abschnitte *Kompetenzbereiche* und *Kompetenzstufen* genau zu lesen und zu verstehen.
2. [Arbeit mit dem Kompetenzenraster](01%20Kompetenzenraster/Arbeit_mit_dem_Kompetenzenraster.md): Ganzes Dokument sorgfältig studieren.
3. [Inputs](00%20Organisatorisches/Inputs.md): Die allgemeinen Informationen (ohne *Zeitplan*) lesen und allfällige Fragen klären.
4. [Lernjournal](00%20Organisatorisches/Lernjournal.md): Das ganze Dokument ist relevant - genau lesen und Fragen klären.

## Szenario für das Modul auswählen
Wählen Sie eines der beiden [Szenarien](02%20Materialien/01-Szenarien.md) für die Modulbearbeitung aus.

## UX Seite zum selbst ausprobieren

[Weshalb User Experience so wichtig ist](https://userinyerface.com)

## Tägliche Zielsetzung im Kompetenzenraster

Wir nutzen das [Miro-Board](https://miro.com/app/board/uXjVPd7-J1E=/?share_link_id=713269328220) um uns kurzfristige Ziele zu setzen. Woran arbeiten wir in den nächsten 2-3 Lektionen? Lege deinen Namen (als Post-It) auf das entsprechenden Kompetenzbereich, an welchen du als nächstes erarbeitest.


