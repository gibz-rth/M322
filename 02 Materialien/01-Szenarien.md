# Szenario für M322
Für dieses Modul haben Sie die Möglichkeit sich für eines von zwei Szenarien zu entscheiden.

Wählen Sie zu Beginn des Moduls das entsprechende Szenario an welchem Sie arbeiten möchten. Dieses sollte während der gesamten Modullaufzeit nicht mehr geändert werden.

## Szenario 1: Herr Rüdisülis Notenverwaltung
Sie kennen die Geschichte aus dem Modul 319. In Moodle finden Sie ganz oben nochmals den Telefonanruf.

Das Ziel dieses Szenarios ist es eine einfach Notenverwaltung zu bauen. Die weiteren Details werden Sie im Rahmen dieses Moduls erarbeiten.


## Szenario 2: Herr Peterhanses Online Shop
Herr Peterhans möchte in einem Onlineshop seine selbstgemachten Hosenträger verkaufen. Sie als Experte werden im Rahmen dieses Moduls das UI für seinen Online Shop erarbeiten.

Sollten Sie lieber etwas anderes als Hosenträger verkaufen, dürfen Sie das natürlich gerne machen.
