# Design Thinking

Wer denkt, Design Thinking sei lediglich etwas für Grossstadt-Hipster, der irrt: Design Thinking hat weder etwas mit hip und cool noch ausschliesslich mit der digitalen Welt zu tun. Vom kleinen Familienbetrieb über das KMU bis zum digitalen Weltkonzern kann Design Thinking von allen genutzt werden, die innovative und kreative Lösungsansätze suchen.

Ist Design Thinking also der magische Knopf für Kreativität? Nein. Design Thinking kann kreative und innovative Lösungsansätze liefern, aber falsch angewendet kann der Prozess auch nur zahllose farbige Post-It-Poster produzieren. Wie funktioniert Design Thinking? Diese Punkte sind zentral.



## Starte mit Empathie

Der potenzielle Benutzer steht immer im Zentrum von Design Thinking. Zu verstehen, welche Bedürfnisse die potenzielle Benutzerin umtreiben, vor welchem Problem sie steht und warum, das ist der entscheidende Schritt an Empathie, an Einfühlung. Dabei ist es hilfreich, vorhandene Daten und Fakten über das Userverhalten zusammenzutragen. Denn erst wenn ich den Benutzer, seine Bedürfnisse und sein Problem wirklich verstanden habe, bin ich in der Lage, überhaupt eine sinnvolle Lösung zu entwickeln. Design Thinking bietet hier die Denkwerkzeuge, um dem auf den Grund zu gehen.

> Aus dem Lab: In einem Projekt ging unser Kunde davon aus, dass Manager die höchste Anzahl Gesprächsminuten pro Monat verbrauchen und wollte sein Marketing darauf ausrichten. Untersuchungen des User-Verhaltens ergaben, dass LKW-Chauffeure weit längere Gesprächszeiten ansammelten.

## Verwende Iteration als Grundprinzip

Mit «Iteration» ist gemeint, dass man eine Idee nimmt, diese testet, daraus lernt, die Idee verbessert, wieder testet… und dazu braucht man ein konkretes Modell dieser einen Idee, einen «Prototyp». Dieser Prototyp wird möglichst rasch konkret gebaut (genannt «Rapid Prototyping»), um bereits mit echten Benutzern testen zu können. Dadurch kann man sehr früh erkennen, was funktioniert und vor allem was nicht – und zwar bevor man die richtig fetten Investitionen tätigt. Zeix hat gute Erfahrungen mit User-Centered Design gemacht, wir bauen einen userzentrierten Prototyp zum Testen, bevor eine Zeile Programm-Code geschrieben wird.

![2](https://www.businessmapping.com/wp-content/uploads/2019/02/PDCA.png.webp)


## Lass Raum zum Denken zu

Eine analytische Herangehensweise zielt häufig von Beginn weg auf eine mögliche Lösung ab – und genau das verhindert, dass man überhaupt auf innovative und kreative Lösungsansätze kommt. Beim Design Thinking hingegen werden die einzelnen Gedankenschritte bewusst auseinandergehalten. Zuerst versucht man das Thema, dann den Benutzer, dessen Bedürfnisse und Probleme zu verstehen. Erst danach widmet man sich der Lösungsfindung und versucht, mit den geeigneten Denkwerkzeugen innert kurzer Zeit ganz viele verschiedene, kreative und innovative Lösungsansätze zu generieren. Daraus wählt man dann den vielversprechendsten aus.


![4](https://zeix.com/wp-content/uploads/2018/10/DSCF0704-2-764x0-c-default.jpg)



## Greife bewusst in den mentalen Werkzeugkasten

Es gibt diverse Tools, genau wie im echten Werkzeugkasten die Rohrzange oder der Fuchsschwanz. Wer an der Heizung rumschraubt, braucht zwingend die Rohrzange, beim Chüngelistall-Selberbauen sicher nicht. Genauso verhält es sich mit dem mentalen Werkzeugkasten namens Design Thinking: Er stellt Denkwerkzeuge zur Verfügung und je nach Problemstellung nimmt man die geeigneten heraus und braucht sie.

> Design Thinking ist kein Prozess, keine Standard-Lösung und keine Ersatzreligion, sondern ein mentaler Werkzeugkasten.


## Wie setzt man Design Thinking ein?

Design Thinking ist auf Workshop-Anwendung ausgerichtet und setzt mit seinen Denkwerkzeugen die Kreativität der Teilnehmer frei. Bei den Teilnehmenden zählt nicht nur die Funktion, sondern wünschenswert sind Neugier, Bereitschaft zur Veränderung und möglichst unterschiedliche Skills.

Die Dauer eines Design Thinking Workshops ist je nach Zielsetzung unterschiedlich. Die Spannbreite geht von drei, vier Stunden bis zu einem fünftägigen Design Sprint, wo auf Basis von Design Thinking ein Entwicklungsprozess abläuft. Google bspw. nutzt [diese Methode](https://designsprintkit.withgoogle.com/) für strategische Positionierungs-Fragen.

Bleibt die Frage, wann ist Design Thinking nicht sinnvoll oder gar frustrierend? Nicht sinnvoll ist Design Thinking, wenn ein Problem bereits eindeutig definiert ist. Und – Design Thinking löst Veränderungen aus, und die sollten wirklich gewünscht und möglich sein. Denn wenn die Umsetzung der entwickelten Lösungsansätze verzögert wird oder versandet, dann schlägt erfahrungsgemäss die hohe Motivation der Teilnehmenden in grosse Frustration um.

Entscheidend ist zudem, mit den Werkzeugen oder eben Denkwerkzeugen gekonnt umzugehen. Weil mit der Rohrzange sollten Sie als Laie besser nicht alleine an der Heizung rumschrauben…



