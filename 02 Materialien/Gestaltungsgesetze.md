# Gestaltungsgesetze

Die Gestaltgesetze der Wahrnehmung beruhen auf Erkenntnissen der Gestaltungspsychologie und können beim Designen von Webanwendungen, Apps und Webseiten dabei helfen ein besonders nutzer-zentriertes Produkt zu entwickeln.

Als Softwareentwickler:in möchte man seinen Kunden Produkte liefern, die sowohl die gewünschten Funktionen beinhalten, als auch die bestmögliche User Experience bieten. Dazu gibt es einiges zu beachten. 
Bei der Erstellung von individueller Software, sollten einige Regeln eingehalten werden um die Bedienung für den Nutzer zu erleichtern. Zum Beispiel sollten Views, also gewisse Abschnitte in einer Anwendung (zum Beispiel ein Login-Formular), nicht überladen werden. Das führt nicht nur zu verkürzten Ladezeiten, sondern auch zu verkürzten Such- beziehungsweise „Nachdenkzeiten“ beim Verwenden der Anwendung. Somit wird die Zeit, in der Anwendung minimiert und das spart Zeit und Geld im Arbeitsalltag.
Dann gibt es auch noch wichtige Erkenntnisse der Gestaltungspsychologie, mit denen man sich vertraut machen sollte, wenn man sich mit dem Designen von Webanwendungen, Apps oder Webseiten beschäftigt. 


## Das Gesetz der Prägnanz

Um eine Anwendung intuitiv zu gestalten muss man sich nicht nur Gedanken machen, wie man dem Nutzer vermitteln kann, wo er etwas hineinschreiben oder hinklicken soll, sondern man muss dem User auch vermitteln, dass eine Interaktion mit der Anwendung überhaupt möglich ist.
Gleichzeitig stellt sich die Frage, wie wir durch gezielte Design-Entscheidungen die Aufmerksamkeit des Nutzers steuern können, dabei hilft uns unter anderem das Gesetz der Prägnanz. 
Wir arbeiten dabei bevorzugt mit Form und Farbe. Buttons in einem System haben dabei immer die selbe Form, die Farbe variiert nur in Ausnahmefällen, wie zum Beispiel dann, wenn es sich um einen Löschen-Button für wichtige Daten handelt. So wird der Nutzer „alarmiert“ und wird darauf aufmerksam gemacht, dass sein Klick schwerwiegende Auswirkungen haben kann. Je mehr Funktionen eine einzelne Ansicht bietet, desto schwieriger ist es, dem Nutzer eine Anwendung zu bieten, die sich so intuitiv wie möglich bedienen lässt.

![1](https://assets-global.website-files.com/61a5f3107556b374f910fb2f/622f4338ce2486c89d88420e_thumb-01.png)

Hier sehen wir wie uns Gmail mit Form und Farbe beim Senden einer Email unterstützt. Wir sehen unten zwar eine Vielzahl an Optionen, die wir klicken könnten, wahrscheinlich wollen wir die Email aber einfach nur absenden und deshalb wird uns diese Option auch auf dem Präsentierteller serviert.

## Das Gesetz der Ähnlichkeit

Wie bereits oben angesprochen, haben Buttons und andere Komponenten in unseren Anwendungen, sowohl dieselbe Farbe, als auch dieselbe Form. 
Das Gesetz der Ähnlichkeit besagt, je mehr Gemeinsamkeiten verschiedene Elemente haben, desto eher werden sie als eine zusammenhängende Einheit wahrgenommen. Daher ist es auch wichtig, auf die Position und die Größe zu achten.

![2](https://assets-global.website-files.com/61a5f3107556b374f910fb2f/622f455dae3d54858bd308d6_02.png)

Natürlich hängt die Breite des Buttons auch vom beinhalteten Text ab. Es wäre komisch, wenn sich die allgemeine Buttonbreite stets am breitesten Button der ganzen Anwendung orientieren würde, oder nicht?

![3](https://assets-global.website-files.com/61a5f3107556b374f910fb2f/622f4514d6b6b9a3df622726_03.png)

Man kann also auch beim Gesetz der Ähnlichkeit overengineeren.

## Das Gesetz der Nähe

Wenn man Elemente gruppiert und Abstände bewusst wählt, bedient mansich am Gesetz der Nähe. Elemente die sich nahe zueinander befinden, werden als zusammengehörig wahrgenommen. Egal, ob es sich um den horizontalen oder vertikalen Abstand handelt. 


![4](https://assets-global.website-files.com/61a5f3107556b374f910fb2f/622f464b196315687966833c_thumb-04.png)

In dieser Grafik erkennt man alleine durch den Abstand der Zeilen, dass es auf der linken Seite eine Überschrift gibt, während sich auf der rechten Seite vier Zeilen von gleichwertigem Content befinden.

![5](https://assets-global.website-files.com/61a5f3107556b374f910fb2f/622f465c1e60d13915dd40bb_thumb-05.png)

Hier haben wir die Struktur einer rudimentären Website. Durch die Abstände sind die einzelnen angedeuteten Sektionen, wie Header, Content und Footer leicht zu erkennen.

![6](https://assets-global.website-files.com/61a5f3107556b374f910fb2f/622f466c1e60d1ff3bdd4a6f_thumb-06.png)

Auch wenn es bei dieser Grafik zwei verschiedene Formen gibt, würde man durch den horizontalen Abstand nicht nach Form gruppieren. Das Gesetz der Nähe zeigt also seine Wirkung. 

## Das Gesetz der Geschlossenheit

Bei diesem Gesetz geht es darum, dass wir darauf trainiert sind Zusammenhänge zu sehen, unabhängig davon ob diese wirklich existieren. Das Gesetz der Geschlossenheit und das oben behandelte Gesetz der Nähe treten in den meisten Fällen gemeinsam auf.

Sehen wir uns das Gesetz der Geschlossenheit zuerst einmal isoliert an. Bei diesem Gesetz geht es darum, dass wir Menschen darauf ausgerichtet sind, in allem was wir sehen, Zusammenhänge zu entdecken. Aufgrund unserer Erfahrung vervollständigen wir einzelne zu geschlossenen Elementen, weil diese greifbarer sind.

![7](https://assets-global.website-files.com/61a5f3107556b374f910fb2f/622f468167930e419f8d71c5_thumb-07.png)

Beim linken Bild sehen wir Quadrate und beim rechten ein „E“.


Auch bei Apps und Webseiten finden sich gewohnte Zusammenhänge oder eben Geschlossenheiten, an die wir uns schon längst gewohnt haben. Auf Webseiten, die diese Geschlossenheiten einhalten finden wir uns besonders schnell zurecht.

![8](https://dealnews.a.ssl.fastly.net/files/uploads/Amazon-Screenshot-1.jpg)

Hier ein Beispiel von Amazon. Links: Darstellung des Produktes, zentral: Eigenschaften und Information des Produktes, rechts: Informationen zum Kauf

Bei diesen 3 Abschnitten ist bereits beim ersten Hinsehen klar, was einen erwartet.

## Das Gesetz der Kontinuität

Bekannte und vor allem gewohnte Formen und Muster vereinfachen es uns, Informationen zu erfassen. Wenn wir uns an das Gesetz der Kontinuität halten, erreichen wir, dass die Nutzer unserer Webseiten oder Apps diese einfach benutzen können.

Ein einfaches Beispiel, wie man es dem Nutzer schwer machen kann, Text zu konsumieren, ist
den Text
zentriert anzuzeigen
und
dabei auch mit den Zeilenlängen zu variieren.

Das Auge muss sich viel mehr anstrengen, um zentrierte Zeilen lesen zu können. Grund dafür ist, dass wir beim Lesen darauf trainiert sind, von einem Zeilenende zum nächsten Zeilenanfang zu springen, sobald wir eine Zeile gelesen haben. Problem bei zentriertem Text: Es gibt kein regelmäßiges Ende und, viel wichtiger noch, keinen regelmäßigen Anfang.

### Antizipieren - Information bewusst darstellen

Vor allem bei der Darstellung von Daten kommt dieses Gesetz zum Einsatz.

Das Ziel der Datenaufbereitung ist den Nutzern Informationen verständlich und einfach zur Verfügung zu stellen. 

Wenn es vom Kunden keine speziellen Vorgaben gibt, sollten wir uns vor der Darstellung der Daten damit beschäftigen, für welche Zielgruppe wir die Daten aufbereiten. Was wollen die Nutzer des Produkts wissen? Wie zeigen wir die Daten initial an und bieten wir mehrere Sortierungsoptionen an?

![9](https://assets-global.website-files.com/61a5f3107556b374f910fb2f/622f46bb6304c9eb57b7b588_thumb-09.png)

Wenn wir diese Gedanken zur Umsetzung mit dem Kunden teilen, liefern wir möglicherweise wichtigen Input, der dem Kunden und schließlich uns hilft, das ideale Produkt für ihn zu schaffen.

![10](https://assets-global.website-files.com/61a5f3107556b374f910fb2f/622f46f486908a2997e61582_thumb-10.png)

Es ist wohl Ansichtssache welche Darstellung den besseren Informationsgehalt liefert. Wenn sich an der Stelle, an der wir die Ansicht bereitstellen, mit hoher Wahrscheinlichkeit die Frage stellen wird, wer die Top 5 Verdiener sind, so macht es bestimmt mehr Sinn die untere Variante zu wählen.

## Das Gesetz des gemeinsamen Schicksals

Hinter diesem dramatischen Namen verbirgt sich, dass Elemente, die sich gemeinsam über den Bildschirm bewegen und jene, die erst sichtbar werden, sobald man mit bestimmten Elementeninteragiert, von uns als zusammengehörig wahrgenommen werden.

An diesen Erkenntnissen bedienen wir uns und setzen deshalb Webelemente wie Accordions oder Tooltips ein. So kann Information dann angezeigt werden, wenn sie wirklich gebraucht wird.

![8](https://www.webmasterpro.de/wp-content/uploads/2021/12/gs_2-1.gif)




