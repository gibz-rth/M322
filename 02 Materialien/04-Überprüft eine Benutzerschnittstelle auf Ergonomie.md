# Überprüft eine Benutzerschnittstelle auf Ergonomie.

Sie haben in den vorangegangen Kompetenzen gelernt was Software Ergonomie ist, wie Sie ein Fenster gestalten sollten und welche Normen es gibt. Ihr nächster Schritte ist nun die Überprüfung genau dieser Regeln, Normen und Richtlinien.

✅ Starten Sie zum ersten Kennenlernen mit der Webseite von [Software Testing Help](https://www.softwaretestinghelp.com/usability-testing-guide/). Der Usability Testing Guid beschreibt die wesentlichen Bestandteile für das Usability Testing.

Versuchen Sie noch weitere Ressourcen zum Thema zu finden und vergleichen Sie die beschriebenen Methoden und Ansätze. Wählen Sie zwei aus, welche Sie genauer anschauen möchten.

✅ Bei [playbookux](https://www.playbookux.com/10-popular-usability-testing-methods/) werden Ihnen 10 Methoden vorgestellt.

## Metriken
Neben den verschiedenen Usability Test Methoden haben sich Metriken etabliert die Sie als Gradmesser für gute Usability hernehmen können.

Metriken sind Kennzahlen/Werte die aufgrund eines standardisierten Verfahrens immer wieder angewendet bzw. ausgewertet werden können.

Wir möchten in diesem Modul die "**System Usability Scale**" sowie das von Google entwickelte **HEART** (Happiness,Engagement,Adoption,Retention,Task success) Framework kennen lernen.

Zusätlich zu diesen beiden Frameworks versuchen wir anhand von **Umfragen** oder **Logfileanalysen** unser UI in Bezug auf Usability zu bewerten.