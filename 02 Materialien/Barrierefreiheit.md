# Barrierefreiheit

Falls Sie schon mal mit Ihrem Fahrrad, Auto oder Motorrad vor einer Bahnschranke standen und sich überlegt haben wie lange es wohl noch dauert bis der Zug verbei ist und ich passieren kann, dann können Sie sich in etwa vorstellen wie es einem Menschen geht der evtl. nicht mehr so gut sieht und sich eine interaktive Webseite anschauen möchte.

Die Webseite bzw. die Bedienung stellt eine Art Barriere dar die kaum überwindbar ist. Unser Ziel zum Thema Barrierefreiheit ist es zu verstehen wo Barrieren entstehen könnten und wie wir aktiv etwas dagegen unternehmen können.

✅
Lesen Sie als Einstieg in das Thema den den Blog Artikel von Markus Lemcke bei [Informatik Aktuell](https://www.informatik-aktuell.de/entwicklung/programmiersprachen/barrierefreie-entwicklung-von-webseiten-software-und-apps.html#:~:text=Barrierefreie%20Softwareentwicklung%20bedeutet%2C%20dass%20eine,anderen%20k%C3%B6rperlichen%20Einschr%C3%A4nkungen%20%E2%80%93%20bedienbar%20ist.). Dieser bezieht sich auf Deutschland, kann aber auf die Schweiz heruntergebrochen werden.

## Normen & Richtlinien
Es gibt verschiedene Normen, Richtlinien und Standards. Fangen Sie doch mit folgenden an:

- [WAI-Aria](https://www.w3.org/WAI/standards-guidelines/aria/)
- [WCAG](https://www.w3.org/WAI/standards-guidelines/wcag/)

## Best Practices
Es haben sich über die Jahre sogenannte Best Practices herausgestellt. Das sind Praktiken die am besten funktionieren und die eine Art Standard darstellen. Wenn sich alle daran halten, funktioniert es.

Für WAI-Aria gibt es dazu eine komplette Webseite die insbesondere auch Patterns für die meisten gängigen UI Elemente aufzeigt. [ARIA Authoring Practices Guide (APG) Home](https://www.w3.org/WAI/ARIA/apg/)
