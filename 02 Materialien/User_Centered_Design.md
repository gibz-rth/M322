# User-Centered Design

Eine gute User Experience ist für digitale Produkte unverzichtbar. Was dies genau bedeutet und wie dieses Ziel mit User-Centered Design erreichen wird , erklären wir anhand von sieben «W-Fragen».

Die Entwicklung von Websites, Business-Applikationen, mobilen Apps, Touch-Screens oder physischen Geräten ist komplex. User-Centered Design ermöglicht, diese Komplexität zu handhaben: Es geht konsequent von den Bedürfnissen der User aus und schafft dadurch klare Prioritäten der Anforderungen. Diese werden visualisiert, denn Visualisierungen werden, anders als abstrakter Text, von allen gleich verstanden.

## Was ist User-Centered Design?

User-Centered Design (UCD) ist eine Methode, bei der die späteren Benutzer (User) von Anfang an einbezogen werden. Dadurch wird sichergestellt, dass der Aufbau, der Inhalt und das Design des Endprodukts massgeblich von den Bedürfnissen, Erwartungen und dem Verständnis der User gesteuert werden. Das digitale Produkt wird von der Benutzerseite (Frontend) aus konzipiert und sehr früh visualisiert. Dadurch sind für die Umsetzung (Front- und Backend) sehr konkrete Vorgaben (Spezifikationen) möglich, weil klar ist, wie die Applikationen, Prozesse und Produkte aussehen und funktionieren sollen.

![1](https://www.seobility.net/de/wiki/images/b/b7/User-Centered-Design.png)


## Warum User-Centered Design?

Der Erfolg eines Produkts hängt davon ab, wie gut es die Benutzer und ihre Ziele unterstützt und wie gut sie sich dabei fühlen. Die Förderung des Nutzens und des Benutzungserlebnisses beim Gebrauch eines Produkts ist zentral: Denn nur wenn Benutzer ein Produkt nutzen wollen, wird das Ziel erreicht, die Attraktivität des Produkts zu steigern. Ökonomisch betrachtet führt UCD zu kürzeren Entwicklungszeiten und somit zu tieferen Entwicklungskosten sowie zu einfacherer Pflege, Wartung und Ausbaubarkeit. Daher ist der Ansatz des UCD der richtige Weg, um kosteneffizient erfolgreiche Produkte zu entwickeln.

## Wie sieht der Prozess aus?

Ein UCD ist ein Prozess, in dem man vom Allgemeinen zum Speziellen vorgeht (Strategie, Analyse, Grobkonzeption, Detailkonzeption) und dabei laufend iteriert. Das heisst: Es werden ähnliche Schritte mehrfach durchlaufen, wobei das Resultat immer mehr verfeinert und verbessert wird. Das Konzept wird während der Entwicklung mittels Usability-Tests oder weiteren geeigneten Methoden direkt mit den künftigen Anwendern aus den Zielgruppen überprüft. Die Ergebnisse des Tests fliessen wiederum ins Konzept ein. Dafür werden Prototypen erstellt, die für die User, aber auch fürs Projektteam, das Management und die technischen Umsetzungspartner das System simulieren. Dabei verstehen wir unter Prototypen die klickbare Visualisierung des künftigen Produkts, nicht eine Erprobung der technischen Möglichkeiten. Dieser Prototyp dokumentiert die Screens und Prozesse und wird ergänzt durch Regelwerke für das Verhalten der Elemente. Dies bildet die Grundlage für die technische Umsetzung, die selbst oder von einem anderen Partner vorgenommen werden kann. In der Regel gehören auch barrierefreie HTML-/CSS-Templates, inhaltliche und grafische Styleguides zu den Lieferobjekten.

![5](https://zeix.com/wp-content/uploads/2011/04/Skizzen-1194x0-c-default.png)
> Frühe Visualisierung im User-Centered-Design-Prozess: Handskizzen der ePaper-App für Tamedia

![6](https://zeix.com/wp-content/uploads/2011/04/Umsetzung-1194x0-c-default.png)
> ... und die entsprechenden Screens nach diversen Feedbackrunden in der Live-Version.

## Wann ist der richtige Zeitpunkt?

UCD bedeutet, ein Projekt aus der Benutzerperspektive zu beginnen und diese Perspektive über den gesamten Entwicklungsprozess hinweg beizubehalten. UCD ist also nicht an einem bestimmten Punkt im Projekt anzusiedeln. Ganz falsch wäre es, erst kurz vor dem Launch die Benutzer einzubeziehen und «noch rasch einen Usability-Test durchzuführen». Dies führt unweigerlich zu grosser Enttäuschung und Frustration, da bestenfalls erkannt wird, wo in der Anfangsphase des Projekts Fehler passiert sind. Für deren Behebung bleibt in diesem Fall keine Zeit mehr. Deshalb sollte das Vorgehen im Idealfall bereits vor der Ausschreibung von Projekten und der Wahl von Projektpartnern berücksichtigt werden.

## Wer bietet User-Centered Design an?

Seriöses und umfassendes UCD wird in der Schweiz von wenigen spezialisierten Firmen angeboten. Zentral ist, dass keine Trennung zwischen den Rollen der Konzepter und den Usability-Experten besteht. Denn um benutzerfreundliche Applikationen entwickeln zu können, müssen Konzepter Verhalten und Bedürfnisse der User wirklich kennen, verstehen und spezifisch erfassen. Umgekehrt nehmen die Resultate von Usability-Tests Einfluss auf das Konzept, das entsprechend angepasst werden muss. Isolierte Usability-Tests ohne fachliche Begleitung laufen Gefahr, nur das Gewünschte zu bestätigen oder kosmetische Anpassungen zu bewirken.

## Wie lange dauert es?

Grob gesagt können Analyse und Grobkonzeption in der Regel in etwa zwei Monaten durchgeführt werden. Für die Detailkonzeption und detaillierte Dokumentation muss mit rund drei weiteren Monaten gerechnet werden. Dies beinhaltet auch die Zeit, die der Auftraggeber für Feedbackrunden, Machbarkeitsprüfungen, Projektmarketing und Entscheidungen einplanen muss. Weil am Ende eines UCD eine detaillierte Spezifikation vorliegt, verkürzt sich der Umsetzungsprozess entsprechend.

![9](https://zeix.com/wp-content/uploads/2011/04/Flow-2-1194x0-c-default.png)
> Dokumentation der Prozesse und Zusammenhänge zwischen des Screens der Tamedia ePaper-App (Ausschnitt)

## Wie viel kostet es?

UCD eignet sich vor allem für mittlere bis grosse Projekte, die über eine gewisse inhaltliche und organisatorische Komplexität verfügen. Richtig durchgeführt kostet ein Projekt mit UCD weniger als ein Projekt mit anderen Software-Entwicklungsmethoden. Die Kosten verschieben sich allerdings. Es ist mehr Aufwand in den frühen Projektphasen nötig, da die Anforderungen und Bedürfnisse viel detaillierter erhoben und beschrieben werden. Dafür nehmen die Kosten gegen Ende des Projekts ab. Und die Wahrscheinlichkeit von Change Requests ist minimal, sowohl während dem laufenden Projekt wie auch bei der Nutzung nach dem Launch.




