# Card-Sorting

Eine gute Informationsarchitektur unterstützt Ihre User, Inhalte schnell und einfach zu finden. Sie führt die Nutzenden effizient zu ihrem Ziel – ein Zeichen für gute UX / Usability und für eine hohe Conversion-Rate.
Bestimmt haben Sie sich schon mal bei der Nutzung einer Webseite oder App verloren gefühlt und sind am Ende Ihrer Suche wieder zurück zur Startseite, um die Reise von vorn zu beginnen. Was Sie erlebt haben, ist das normale Resultat einer fehlerhaften Informationsarchitektur.
Statt dem realen mentalen Modell der Zielgruppe zu entsprechen, bilden Navigationen häufig „nur“ die interne Sicht auf die Customer Journey von Kunden ab. Im schlimmsten Fall wird Ihre Navigation so zum Abbruchgrund oder lässt beim Besuch der Website täglich frustrierte User zurück.


## Wann führt man ein Card-Sorting durch?

**Kommt drauf an! – frühe vs. späte Produktentwicklungsphasen**

Card-Sorting wird um UX-Design vorwiegend in der Analyse- oder Konzeptionsphase eingesetzt. Aber auch später, bspw. wenn Probleme mit der bestehenden Informationsarchitektur vermutet werden, kann Card-Sorting hilfreich sein. Ist man noch in einem frühen Stadium der Produktentwicklung, greift man in der Regel zu einem offenen Card-Sorting, da es einen generativen Ansatz verfolgt – es bringt neue Gruppierungsideen hervor.
Durch Card-Sortings in frühen Entwicklungsstadien können Änderungen noch kostengünstig am Wireframe oder im Prototyp vorgenommen werden. In späteren Produktentwicklungsphasen oder wenn ein Produkt schon released ist, nutzt man häufig geschlossenes Card-Sorting und Reverse-Card-Sorting (auch: Tree-Testing) mit einem evaluierenden Ansatz.

## Offenes Card-Sorting (generativer Ansatz)

Bei einem offenen Card-Sorting sortieren die Testpersonen Seiten, Inhalte, Begriffe, Informationen oder Funktionen in für sie zusammengehörige Gruppen. Es bietet damit einen sehr guten Einblick in das mentale Modell der Zielgruppe. Das offene Card-Sorting wird idealerweise noch in der Konzeptionsphase durchgeführt, aber ein Content-Audit sollte zu diesem Punkt schon stattgefunden haben.

![4](https://userlutions.com/wp-content/uploads/2020/11/card-sorting-offen-e1604487161508.png.webp)

Die gebildeten Cluster werden auf Gemeinsamkeiten untersucht, um darauf aufbauend eine Empfehlung für eine Informationsarchitektur zu geben. Da die Anzahl der von den Testpersonen als zusammengehörig empfundenen Gruppen sehr stark variieren kann, benötigt man für valide Ergebnisse eine grosse Stichprobe.

## Geschlossenes Card-Sorting (evaluierenden Ansatz)

Bei einem geschlossenen Card-Sorting sortieren Testpersonen Seiten, Inhalte, Begriffe, Informationen oder Funktionen in vorgegebene Gruppen. Geschlossenes Card-Sorting wird häufig eingesetzt, wenn UX-Tests / Usability-Tests, Analytics oder Analyse der Suchanfragen gezeigt haben, dass es Probleme bei der Navigation gibt, die oberste Ebene der Navigation jedoch nicht verändert werden kann.

![5](https://userlutions.com/wp-content/uploads/2020/11/card-sorting-geschlossen-e1604487134388.png.webp)

## Reverse-Card-Sorting / Treetesting (evaluierender, iterativer Ansatz)

Beim Reverse-Card-Sorting existiert ebenfalls bereits eine Informationsarchitektur. Allerdings sortieren die Testpersonen hier keine Begriffe, sondern erhalten die Aufgabe, nach bestimmten Informationen zu suchen. Die Suche findet in der rein hierarchischen Informationsstruktur statt (meist noch ohne das UI-Design). Dies bietet den Vorteil, dass der kognitive Prozess eher der eigentlichen Herangehensweise entspricht.
Ausgewertet wird die Erfolgsquote (Information gefunden), der direkte Weg (ohne Rückschritte) sowie die Zeit. Die Methode gibt Einblicke in das Navigationsverhalten von Usern. Reverse-Card-Sorting wird häufig eingesetzt, um zwei verschiedene Informationsarchitekturen miteinander zu vergleichen oder nach Veränderungen der Informationsarchitektur den Erfolg zu benchmarken.

![7](https://userlutions.com/wp-content/uploads/2020/11/card-sorting-reverse-e1604487088908.png.webp)

## Wie führt man das Card-Sorting durch?
**Online vs. Offline – und das ist nicht die einzige Frage!**

Ein Card-Sorting kann sowohl offline als auch online durchgeführt werden. Da quantitative Daten zur Validierung erhoben werden wollen, hat sich der Trend durchgesetzt, Card-Sorting online durchzuführen. Online-Card-Sorting ist bei großen Stichproben in aller Regel kostengünstiger und schneller durchführbar, da u.a. die Daten sofort digital zur Analyse vorliegen und die Kosten für die Incentivierung niedriger sind.
Ein grosser, nicht zu unterschätzender Nachteil, ist aus meiner Sicht jedoch der Verzicht auf die UX-Methode „Laut denken“. Die meisten Online-Card-Sorting-Tools bieten keine Möglichkeit, dass Testpersonen ihre Gedanken und Fragen zu den einzelnen Karten kommentieren. Auch kann das Verhalten nicht beobachtet werden, was oft zusätzliche Erkenntnisse bietet. Im schlimmsten Fall liefert ein rein quantitativ durchgeführtes Card-Sorting gar keine neuen Erkenntnisse.

**Qualitativ vs. quantitativ – Die Kombination macht’s!**

Es empfiehlt sich eine quantitative und qualitative Herangehensweise zu kombinieren. Also mit einer grossen Stichprobe ein unmoderiertes Online-Card-Sorting für valide quantitative Ergebnisse durchzuführen und mit einer wesentlich kleineren Stichprobe das Card-Sorting mit der Laut-denken-Methode zu kombinieren, um qualitative Insights zu generieren. Letzteres könnte klassisch in einer moderierten (synchronen) Card-Sorting-Session vor Ort stattfinden.
